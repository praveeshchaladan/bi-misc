=> The access log required for functionality of this script should be of "yyyy-mm-dd hh:ss,INITIAL_LOGIN/REFRESH_TOKEN_LOGIN,app_code,user_id,url" format.
=> log file must be in ascending order of date


Steps to Execute the Scripts

1) Update the internal/external users list with new registration data

    $ python script_1.py <Registration File>

 => it will update the id_cc_isExternal.csv file
 => run this command only once as it will keep adding the same data to id_cc_isExternal.csv file
 => this also gives you the total number of new registrations added

2) Generate country wise reports

    $ python script.py <Access Log File>

 => it will generate a new directory and give you 6 output files in it

3) Run the main script on access log (for total and application wise data)

    $ python mainscript.py <Access Log File>

4) Run update script to update the all user reference files.

    $ python update.py <Access Log File>

 => this will update all the reference users*.csv files.



                                          REFERENCE FILES:

1) all_the_users.csv:

This file contains the list of all the (user and app) first entry.
It helps us in finding the new users.
It has to be updated after each log file operation.

2) users_this_year.csv:

all the unique (user and app) entries of the year are stored in this file
it helps us find about the new user app combination of year

3) users_that_moment_monthly.csv, users_that_moment_weekly.csv, users_that_moment.csv:

this contains all the unique (user and app) combination monthly, weekly and daily respectively.

4) id_cc_isExternal.csv:

contains the country and external/Internal information of each id

5) client_id_app_matching.csv and URL_Mapping.csv:

contains the client id and url mapping to app name.
=> when we can't find the app name through these mappings we take the application name to be "no_app"



                                                  LOGIC:
