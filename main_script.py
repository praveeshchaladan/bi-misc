import sys
import csv
import datetime
import os

#This script is for counting new users and active users.
#The access log required for functionality of this script should be of "yyyy-mm-dd time,INITIAL_LOGIN/REFRESH_TOKEN_LOGIN,app_code,user_id,url" format.
#Also log file must be in ascending order of date
#This script uses the outputs of this code as the input in the processing of next log file,so don't run this script twice for one log file as it will mess up the output.
#all_the_users.csv,users_that_moment.csv,users_that_moment_weekly and users_that_moment_weekly are constantly updated after every program.

def get_year(date):
	a = date.split("-")
	b = a[0]
	return b

def get_week(date): #this returns the week number
	a = date.split("-")
	b = int(a[0])
	c = int(a[1])
	d = int(a[2])
	day = datetime.datetime(b,c,d)
	day += datetime.timedelta(days = 1)
	e = day.isocalendar()[1]
	if (e!=53 or c==12):
		f = a[0] + str(e)
	else:
		f = str(b - 1) + str(e)
	return f

def get_month(date): #this returns the month
	a = date.split("-")
	b = int(a[1])
	if (b==1): return "01" + a[0]
	elif (b==2): return "02" + a[0]
	elif (b==3): return "03" + a[0]
	elif (b==4): return "04" + a[0]
	elif (b==5): return "05" + a[0]
	elif (b==6): return "06" + a[0]
	elif (b==7): return "07" + a[0]
	elif (b==8): return "08" + a[0]
	elif (b==9): return "09" + a[0]
	elif (b==10): return "10" + a[0]
	elif (b==11): return "11" + a[0]
	elif (b==12): return "12" + a[0]
	else :return None

def is_First_time(id,app,lis): #is it a new user for a certain application
	for x in lis:#here lis is in id,country code,app format
		if x[0]==id and x[2]==app:
			return False
			break
		elif x[0]==id and app=="not_needed":#if we only need to search the user not the application he was using
			if x[2] not in blocked_list:
				return False
				break
	return True

def isExternal(id): #is the user external or internal
	a = dict.get(id)#here dict is a dictionary which has id as the key and country code and external/internal as value
	if a:
		if (a[1]=="External"):
			return True
		else:
			return False
	else:
		return True

def app_name(com,url): #returns the app of the user
	a = dict_2.get(com)#here com is the app code
	for x in list(dict_1.keys()):
			if x in url:
				return dict_1.get(x)
				break
	if a:
		return a
	else: return "no_app"

def new_active_overall(filename,lis,lis1):
	p = list(lis)		#this list helps us find new users
	dict = {}			#this will be returned at the end of program
	q = list(lis1)		#this will help us in finding the active users
	f = open(filename, 'r')
	text = f.readlines()
	apparr = []
	dict["date"] = ["Active_Users","New_Users"]

	if text and lis1:
		e = text[0].split(" ")
		d = q[0]
		dict[e[0]] = [0,0]
		if (e[0] != d[1]):#this if statement is for checking that active user list that we got is of same time as of the access log
			del q[:]

	for line in text:
		dat1 = line.split(",")
		id = dat1[3]
		dat = line.split(" ")
		date = dat[0]
		str1 = dat1[2]
		url = dat1[4]
		app = app_name(str1,url)
		if (app not in blocked_list) :
			if isExternal(id):
				if date in dict.keys():
					if is_First_time(id,"not_needed",q):
						a = dict[date]
						a[0] = a[0]+1 #active users increased by 1
						dict[date] = a
						q.append([id,date,""])
						if is_First_time(id,"not_needed",p):
							b = dict[date]
							b[1] = b[1]+1 #new users increased by one
							dict[date] = b
							p.append([id,date,""])
				else:
					q = [[id,date,""]]
					if is_First_time(id,"not_needed",p):
						dict[date] = [1,1]	#Active and new users both one
						p.append([id,date,""])
					else: dict[date] = [1,0] #Active users 1, new users 0
	return dict

def new_active_overall_weekly(filename,lis,lis1):
	p = list(lis)		#this list helps us find new users
	dict = {}			#this will be returned at the end of program
	q = list(lis1)		#this will help us in finding the active users
	f = open(filename, 'r')
	text = f.readlines()
	dict["week"] = ["Active_Users","New_Users"]

	if text and lis1:
		e = text[0].split(" ")
		d = q[0]
		r = get_week(e[0])
		dict[r] = [0,0]
		if (get_week(e[0]) != d[1]):#this if statement is for checking that active user list that we got is of same time as of the access log
			del q[:]

	for line in text:
		dat1 = line.split(",")
		id = dat1[3]
		dat = line.split(" ")
		date = dat[0]
		week = get_week(date)
		str1 = dat1[2]
		url = dat1[4]
		app = app_name(str1,url)
		if (app not in blocked_list) :
			if isExternal(id):
				if week in dict.keys():
					if is_First_time(id,"not_needed",q):
						a = dict[week]
						a[0] = a[0]+1 #active users increased by 1
						dict[week] = a
						q.append([id,week,""])
						if is_First_time(id,"not_needed",p):
							b = dict[week]
							b[1] = b[1]+1 #new users increased by one
							dict[week] = b
							p.append([id,date,""])
				else: #the week has changed
					q = [[id,week,""]]
					if is_First_time(id,"not_needed",p):
						dict[week] = [1,1]	#Active and new users both one
						p.append([id,date,""])
					else: dict[week] = [1,0] #Active users 1, new users 0
	return dict

def new_active_overall_monthly(filename,lis,lis1):
	p = list(lis)		#this list helps us find new users
	dict = {}			#this will be returned at the end of program
	q = list(lis1)		#this will help us in finding the active users
	f = open(filename, 'r')
	text = f.readlines()
	dict["month"] = ["Active_Users","New_Users"]
	if text and lis1:
		e = text[0].split(" ")
		d = q[0]
		r = get_month(e[0])
		dict[r] = [0,0]
		if (get_month(e[0]) != d[1]):#this if statement is for checking that active user list that we got is of same time as of the access log
			del q[:]

	for line in text:
		dat1 = line.split(",")
		id = dat1[3]
		dat = line.split(" ")
		date = dat[0]
		month = get_month(date)
		str1 = dat1[2]
		url = dat1[4]
		app = app_name(str1,url)
		if (app not in blocked_list) :
			if isExternal(id):
				if month in dict.keys():
					if is_First_time(id,"not_needed",q):
						a = dict[month]
						a[0] = a[0]+1 #active users increased by 1
						dict[month] = a
						q.append([id,month,""])
						if is_First_time(id,"not_needed",p):
							b = dict[month]
							b[1] = b[1]+1 #new users increased by one
							dict[month] = b
							p.append([id,date,""])
				else: #the week has changed
					q = [[id,month,""]]
					if is_First_time(id,"not_needed",p):
						dict[month] = [1,1]
						#Active and new users both one
						p.append([id,date,""])
					else: dict[month] = [1,0] #Active users 1, new users 0
	#print(dict)
	return dict

def new_active_overall_yearly(filename,lis,lis1):
	p = list(lis)		#this list helps us find new users
	dict = {}			#this will be returned at the end of program
	q = list(lis1)		#this will help us in finding the active users
	f = open(filename, 'r')
	text = f.readlines()
	dict["year"] = ["Active_Users","New_Users"]

	if text and lis1:
		e = text[0].split(" ")
		d = q[0]
		r = get_year(e[0])
		dict[r] = [0,0]
		if (get_year(e[0]) != d[1]):#this if statement is for checking that active user list that we got is of same time as of the access log
			del q[:]

	for line in text:
		dat1 = line.split(",")
		id = dat1[3]
		dat = line.split(" ")
		date = dat[0]
		year = get_year(date)
		str1 = dat1[2]
		url = dat1[4]
		app = app_name(str1,url)
		if (app not in blocked_list) :
			if isExternal(id):
				if year in dict.keys():
					if is_First_time(id,"not_needed",q):
						a = dict[year]
						a[0] = a[0]+1 #active users increased by 1
						dict[year] = a
						q.append([id,year,""])
						if is_First_time(id,"not_needed",p):
							b = dict[year]
							b[1] = b[1]+1 #new users increased by one
							dict[year] = b
							p.append([id,date,""])
				else: #the week has changed
					q = [[id,year,""]]
					if is_First_time(id,"not_needed",p):
						dict[year] = [1,1]	#Active and new users both one
						p.append([id,date,""])
					else: dict[year] = [1,0] #Active users 1, new users 0
	return dict

def app_active_new(filename,lis,lis1):
	p = list(lis)		#this list helps us find new users
	dict = {}			#this will be returned at the end of program
	q = list(lis1)		#this will help us in finding the active users
	f = open(filename, 'r')
	text = f.readlines()
	arr = []
	dict["date;app"] = ["Active_Users","New_Users"]

	#rest of the code is similar to the fuction above
	if text and lis1:
		e = text[0].split(" ")
		d = q[0]
		arr.append(e[0])
		if (e[0] != d[1]):
			del q[:]

	for line in text:
		dat1 = line.split(",")
		id = dat1[3]
		dat = line.split(" ")
		date = dat[0]
		str1 = dat1[2]
		url = dat1[4]
		app = app_name(str1,url)
		if app not in blocked_list :
			boo = isExternal(id)
			coo = not boo
			if (boo or (coo and app == "Ecoreal XL")):
				var1 = date + ";" + app
				if var1 in dict.keys():
					if is_First_time(id,app,q):
						a = dict[var1]
						a[0] = a[0]+1
						dict[var1] = a
						q.append([id,date,app])
						if is_First_time(id,app,p):
							b = dict[var1]
							b[1] = b[1]+1
							dict[var1] = b
							p.append([id,date,app])
				else:
					if date in arr:
						if is_First_time(id,app,q):
							q.append([id,date,app])
							if is_First_time(id,app,p):
								dict[var1] = [1,1]
								p.append([id,date,app])
							else: dict[var1] = [1,0]
					else:
						arr.append(date)
						q =[[id,date,app]]
						if is_First_time(id,app,p):
							dict[var1] = [1,1]
							p.append([id,date,app])
						else: dict[var1] = [1,0]

	return dict

def app_active_new_weekly(filename,lis,lis1):
	p = list(lis)		#this list helps us find new users
	dict = {}			#this will be returned at the end of program
	q = list(lis1)		#this will help us in finding the active users
	f = open(filename, 'r')
	text = f.readlines()
	arr = []
	dict["week;app"] = ["Active_Users","New_Users"]

	#rest of the code is similar to the fuction above

	if text and lis1:
		e = text[0].split(" ")
		d = q[0]
		arr.append(get_week(e[0]))
		if (get_week(e[0]) != d[1]):
			del q[:]

	for line in text:
		dat1 = line.split(",")
		id = dat1[3]
		dat = line.split(" ")
		date = dat[0]
		str1 = dat1[2]
		url = dat1[4]
		app = app_name(str1,url)
		week = get_week(date)
		if app not in blocked_list:
			boo = isExternal(id)
			coo = not boo
			if (boo or (coo and app == "Ecoreal XL")):
				var1 = week + ";" + app
				if var1 in dict.keys():
					if is_First_time(id,app,q):
						a = dict[var1]
						a[0] = a[0]+1
						dict[var1] = a
						q.append([id,week,app])
						if is_First_time(id,app,p):
							b = dict[var1]
							b[1] = b[1]+1
							dict[var1] = b
							p.append([id,date,app])
				else:
					if week in arr:
						if is_First_time(id,app,q):
							q.append([id,week,app])
							if is_First_time(id,app,p):
								dict[var1] = [1,1]
								p.append([id,date,app])
							else: dict[var1] = [1,0]
					else:
						arr.append(week)
						q =[[id,week,app]]
						if is_First_time(id,app,p):
							dict[var1] = [1,1]
							p.append([id,date,app])
						else: dict[var1] = [1,0]

	return dict

def app_active_new_monthly(filename,lis,lis1):
	p = list(lis)		#this list helps us find new users
	dict = {}			#this will be returned at the end of program
	q = list(lis1)		#this will help us in finding the active users
	f = open(filename, 'r')
	text = f.readlines()
	arr = []
	dict["month;app"] = ["Active_Users","New_Users"]

	#rest of the code is similar to the fuction above
	if text and lis1:
		e = text[0].split(" ")
		d = q[0]
		arr.append(get_month(e[0]))
		if (get_month(e[0]) != d[1]):
			del q[:]

	for line in text:
		dat1 = line.split(",")
		id = dat1[3]
		dat = line.split(" ")
		date = dat[0]
		str1 = dat1[2]
		url = dat1[4]
		app = app_name(str1,url)
		month = get_month(date)
		if app not in blocked_list:
			boo = isExternal(id)
			coo = not boo
			if (boo or (coo and app == "Ecoreal XL")):

				var1 = month + ";" + app
				if var1 in dict.keys():
					if is_First_time(id,app,q):
						a = dict[var1]
						a[0] = a[0]+1
						dict[var1] = a
						q.append([id,month,app])
						if is_First_time(id,app,p):
							b = dict[var1]
							b[1] = b[1]+1
							dict[var1] = b
							p.append([id,date,app])
				else:
					if month in arr:
						if is_First_time(id,app,q):
							q.append([id,month,app])
							if is_First_time(id,app,p):
								dict[var1] = [1,1]
								p.append([id,date,app])
							else: dict[var1] = [1,0]
					else:
						arr.append(month)
						q =[[id,month,app]]
						if is_First_time(id,app,p):
							dict[var1] = [1,1]
							p.append([id,date,app])
						else: dict[var1] = [1,0]

	return dict

def main():

	vae = str(datetime.datetime.now())
	vaes = vae[:10]
	folder = vaes+"/"
	os.makedirs(os.path.dirname(folder), exist_ok=True)
	print("(o_o)")
	args = sys.argv[1:]

	if not args:
		print ('NO FILE')
		sys.exit(1)

	file = args[0]
	A4 = app_active_new(file,All_the_users,Users_that_day)
	print("(o_o)")
	A3 = new_active_overall_monthly(file,All_the_users,Users_that_month)
	print("(o_o)")
	A1 = new_active_overall(file,All_the_users,Users_that_day)
	print("(o_o)")
	A2 = new_active_overall_weekly(file,All_the_users,Users_that_week)
	print("(o_o)")
	A5 = app_active_new_weekly(file,All_the_users,Users_that_week)
	print("(o_o)")
	A6 = app_active_new_monthly(file,All_the_users,Users_that_month)
	print("(o_o)")
	A7 = new_active_overall_yearly(file,All_the_users,Users_this_year)
	print("(o_o)")

	ofile1  = open(vaes +"/total-daily.csv", "w+" , encoding = "utf8", newline='')
	writer1 = csv.writer(ofile1, delimiter=";")
	ofile2  = open(vaes +"/total-weekly.csv", "w+" , encoding = "utf8", newline='')
	writer2 = csv.writer(ofile2, delimiter=";")
	ofile3  = open(vaes +"/total-monthly.csv", "w+" , encoding = "utf8", newline='')
	writer3 = csv.writer(ofile3, delimiter=";")
	ofile4  = open(vaes +"/app-daily.csv", "w+" , encoding = "utf8", newline='')
	writer4 = csv.writer(ofile4, delimiter=";")
	ofile5  = open(vaes +"/app-weekly.csv", "w+" , encoding = "utf8", newline='')
	writer5 = csv.writer(ofile5, delimiter=";")
	ofile6  = open(vaes +"/app-monthly.csv", "w+" , encoding = "utf8", newline='')
	writer6 = csv.writer(ofile6, delimiter=";")
	ofile7  = open(vaes +"/total-yearly.csv", "w+" , encoding = "utf8", newline='')
	writer7 = csv.writer(ofile7, delimiter=";")

	print("******()*******")
	for q in A1.keys():
		u = A1[q]
		writer1.writerow([q,u[0],u[1]])
	print("(o_o)")
	for w in A2.keys():
		i = A2[w]
		writer2.writerow([w,i[0],i[1]])
	print("(o_o)")
	for y in A3.keys():
		k = A3[y]
		writer3.writerow([y,k[0],k[1]])
	for e in A6.keys():
		o = A6[e]
		temp = e.split(";")
		writer6.writerow([temp[0],temp[1],o[0],o[1]])
	for r in A4.keys():
		p = A4[r]
		temp = r.split(";")
		writer4.writerow([temp[0],temp[1],p[0],p[1]])
	for t in A5.keys():
		l = A5[t]
		temp = t.split(";")
		writer5.writerow([temp[0],temp[1],l[0],l[1]])
	for c in A7.keys():
		l = A7[c]
		writer7.writerow([c,l[0],l[1]])

	print("(o_o)")

	print("******()*******")

if __name__ == '__main__':


	blocked_list = ["no_app","csmdemo","easyline","Ecoreal MV","Ecoreal MV - NPR","Facility Insight","MySchneider","MySchneider - NPR","MySchneider Retailer","myschneider3dlive","mysmartoperator","omega","Rapsody","resiplatform","service-manager","SR","SR - NPR","smartpanel"]

	dict = {} #this dictionary is for users information from reg data
	with open("id_cc_isExternal.csv","r",encoding="utf8") as f:
		reader = csv.reader(f,delimiter = ";")
		for row in reader:
			a = row[0]
			a.lower()
			dict[a] = [row[1],row[2]]
	f.close()

	dict_1 = {} #this maps url to app
	with open("URL_Mapping.csv","r",encoding="utf8") as f:
		reader = csv.reader(f,delimiter = ";")
		for row in reader:
			a = row[0]
			a.lower()
			dict_1[a] = row[1]
	f.close()

	dict_2 = {} #this maps clien_id to app
	with open("client_id_app_matching.csv","r",encoding="utf8") as f:
		reader = csv.reader(f,delimiter = ";")
		for row in reader:
			a = row[0]
			a.lower()
			dict_2[a] = row[1]
	f.close()

	All_the_users = [] #this array contains all the users who have appeared in access logs
	f = open("all_the_users.csv", 'r')
	reader = csv.reader(f,delimiter = ";")
	for row in reader:
		All_the_users.append([row[0],row[1],row[2]])
	f.close()

	Users_that_day = []
	f = open("users_that_moment.csv", 'r')
	reader = csv.reader(f,delimiter = ";")
	for row in reader:
		Users_that_day.append([row[0],row[1],row[2]])
	f.close()

	Users_this_year = []
	f = open("users_this_year.csv", 'r')
	reader = csv.reader(f,delimiter = ";")
	for row in reader:
		Users_this_year.append([row[0],row[1],row[2]])
	f.close()

	Users_that_week = []
	f = open("users_that_moment_weekly.csv", 'r')
	reader = csv.reader(f,delimiter = ";")
	for row in reader:
		Users_that_week.append([row[0],row[1],row[2]])
	f.close()

	Users_that_month = []
	f = open("users_that_moment_monthly.csv", 'r')
	reader = csv.reader(f,delimiter = ";")
	for row in reader:
		Users_that_month.append([row[0],row[1],row[2]])
	f.close()

	main()
