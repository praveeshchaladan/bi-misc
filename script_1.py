import csv
import sys

def main(lis):
  # This command-line parsing code is provided.
  # Make a list of command line arguments, omitting the [0] element
  # which is the script itself.
	args = sys.argv[1:]

	if not args:
		print ('NO FILE')
		sys.exit(1)

	dict = {}
	for files in args:


		with open(files , "r",encoding="utf8") as f:
			count = 0
			reader = csv.reader(f,delimiter = ";")
			output_file = "id_cc_isExternal.csv"
			ofile  = open(output_file, "w" , encoding = "utf8", newline='')
			writer = csv.writer(ofile, delimiter=";")
			for row in reader:
				a = row[0]
				b = row[1].lower()
				c = row[4].lower()
				d = row[5]
				de = d[6:]
				krow = [row[0],row[7]]
				if (len(de) == 4):
					if (de in dict.keys()) :
						dict[de] += 1
					else: dict[de] = 1

				if ("Id" in a):
					krow = ['Id', 'Country', 'Internal/External']
					#print (krow)
				elif (("SESA" in a) or ("@schneider" in b) or (".schneider" in b) or ("mphasis" in b)):
					krow.append('Internal')
					count += 1
					lis.append(krow)
					#print (krow)
				elif((c == "=s=") or ("=se=" in c) or (c=="se") or (c=="sch") or (c=="seipl") or (c=="seil") or (c=="schn") or (c=="DCES") or (c=="sei")):
					krow.append('Internal')
					lis.append(krow)
					count+= 1
					#print(krow)
				elif(("schneider" in c) or ("schnider" in c) or ("schnieder" in c) or ("schneidr" in c) or ("schneide" in c) or ("schnaider" in c) or ("mphasis" in c) or ("ithink" in c) or ("hardis" in c) or ("thales" in c) or ("globallogic" in c) or ("global logic" in c)):
					krow.append('Internal')
					lis.append(krow)
					count +=1
					#print(krow)
				else:
					krow.append('External')
					lis.append(krow)
					#print (krow)
			print('*_*')

			for x in lis:
				writer.writerow(x)
			for y in dict.keys():
				print (str(dict[y]) + " new registrations in " + y)
			print(" ")	
			print(str(count) + " users are internal in this file")
			f.close()

if __name__ == '__main__':
	arr = []
	with open("id_cc_isExternal.csv","r",encoding="utf8") as f:
		reader = csv.reader(f,delimiter = ";")
		for row in reader:
			arr.append([row[0],row[1],row[2]])
	#	print (arr)
	f.close()
	main(arr)
