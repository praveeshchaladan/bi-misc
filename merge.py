import sys
from datetime import datetime
from datetime import timedelta
import glob
import os

def merge(dict,a,b):
    dict_1 = {}
    c = a.split("-")
    d = b.split("-")
    date1 = datetime(int(c[0]),int(c[1]),int(c[2]))
    date2 = datetime(int(d[0]),int(d[1]),int(d[2]))
    if(date1 != date2):
        delta = date2 - date1
        for i in range(delta.days + 1):
            date = str(date1 + timedelta(days=i))[:10]
            print(date)
            if date in dict.keys():
                dict_1[date] = dict[date]
            else:
                print("invalid date")
                sys.exit()
    else:
        if a in dict.keys():
            dict_1[a] = dict[a]
        else:
            print("invalid date")
            sys.exit()
    fil = (a + " "+ b +".log")
    f = open(fil,'w')
    for x in dict_1.keys():
        for y in dict_1[x]:
            f.write(x+" "+y)
    f.close()

def main():
    args = sys.argv[1:]


    if not args:
        print ('NO FILE')
        sys.exit(1)
    directory = args[1]
    date1 = args[2]
    date2 = args[3]

    dict = {}
    #print(directory)
    all_files = glob.glob(directory+"\\*")
    for file in all_files:
        f = open(file,'r')
        text = f.readlines()
        for line in text:
            dat1 = line.split(" ")
            date = dat1[0]
            if date in dict.keys():
                a = dict[date]
                a.append(dat1[1])
                dict[date] = a
            else:
                dict[date] = [dat1[1]]
        f.close()
    #print(dict.keys())
    merge(dict,date1,date2)

if __name__ == '__main__':
	main()
