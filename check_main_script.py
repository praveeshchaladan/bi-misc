import sys
import csv
import datetime

#This script is for counting new users and active users. 
#The access log required for functionality of this script should be of "yyyy-mm-dd time,INITIAL_LOGIN/REFRESH_TOKEN_LOGIN,app_code,user_id,url" format.
#Also log file must be in ascending order of date
#This script uses the outputs of this code as the input in the processing of next log file,so don't run this script twice for one log file as it will mess up the output.
#all_the_users.csv,users_that_moment.csv,users_that_moment_weekly and users_that_moment_weekly are constantly updated after every program. 


def get_week(date): #this returns the week number
	a = date.split("-")
	b = int(a[0])
	c = int(a[1])
	d = int(a[2])
	e = datetime.date(b, c, d).isocalendar()[1]
	if (e!=53 or c==12):
		f ="week no. "+ str(e)+ " of "+a[0]
	else:
		f ="week no. "+ str(e)+ " of "+str(b - 1)
	return f

def is_First_time(id,app,lis): #is it a new user for a certain application
	for x in lis:#here lis is in id,country code,app format
		if x[0]==id and x[2]==app:
			return False
			break
		elif x[0]==id and app=="not_needed":#if we only need to search the user not the application he was using
			return False
			break
	return True

def isExternal(id): #is the user external or internal
	a = dict.get(id)#here dict is a dictionary which has id as the key and country code and external/internal as value
	if a: 
		if (a[1]=="External"): 
			return True
		else: 
			return False
	else: 
		return True

def app_name(com,url): #returns the app of the user
	a = dict_2.get(com)#here com is the app code
	for x in list(dict_1.keys()):
			if x in url:
				return dict_1.get(x)
				break		
	if a: 
		return a
	else: return "no_app"

def new_active_overall(filename,lis,lis1):
	p = list(lis)		#this list helps us find new users
	dict = {}			#this will be returned at the end of program
	q = list(lis1)		#this will help us in finding the active users 
	f = open(filename, 'r')
	text = f.readlines()
			
	ofile  = open("AZ1.csv", "w" , encoding = "utf8", newline='')
	writer = csv.writer(ofile, delimiter=";")
	
	
	if text and lis1:
		e = text[0].split(" ")
		d = q[0]
		if (get_week(e[0]) != d[1]):#this if statement is for checking that active user list that we got is of same time as of the access log
			del q[:]
	
	for line in text:
		dat1 = line.split(",")
		id = dat1[3]
		dat = line.split(" ")
		date = dat[0]
		str1 = dat1[2]
		url = dat1[4]
		week = get_week(date)
		app = app_name(str1,url)
		boo = isExternal(id)
		if (app not in blocked_list) :
			if (boo or ((not boo) and (app == "Ecoreal XL"))):
				if week in dict.keys():
					if is_First_time(id,app,q):
						a = dict[week]
						a[0] = a[0]+1 #active users increased by 1
						dict[week] = a 
						q.append([id,week,app])
						if is_First_time(id,app,p):
							b = dict[week]
							b[1] = b[1]+1 #new users increased by one 
							dict[week] = b
							writer.writerow([week,id,app,"this user is a new and active user for this application this week"])
							p.append([id,date,app])
						else: writer.writerow([week,id,app,"active user but not new user"])
					else: writer.writerow([week,id,app,"this users has come this week before"])
				else: #the week has changed
					q = [[id,week,""]]
					if is_First_time(id,app,p):
						dict[week] = [1,1]	#Active and new users both one			
						p.append([id,date,app])	
						writer.writerow([week,id,app,"this user is a new and active user for this application this day"])
					else: 
						dict[week] = [1,0] #Active users 1, new users 0
						writer.writerow([week,id,app,"this user is a new and active user for this application this day"])
			else: writer.writerow([week,id,app,"user internal"])
		else: 		
			writer.writerow([week,id,app,"app not in DCES"])

	return dict


def main():

	
	args = sys.argv[1:]

	if not args:
		print ('NO FILE')
		sys.exit(1)

	file = args[0]
	new_active_overall(file,All_the_users,Users_that_week)
	
if __name__ == '__main__':
	

	blocked_list = ["no_app","csmdemo","easyline","Ecoreal MV","Ecoreal MV - NPR","Facility Insight","MySchneider","MySchneider - NPR","MySchneider Retailer","myschneider3dlive","mysmartoperator","omega","Rapsody","resiplatform","service-manager","SR","SR - NPR","smartpanel"]
	
	dict = {} #this dictionary is for users information from reg data
	with open("id_cc_isExternal.csv","r",encoding="utf8") as f:
		reader = csv.reader(f,delimiter = ";")
		for row in reader:
			a = row[0]
			a.lower()
			dict[a] = [row[1],row[2]]
	f.close()
	
	dict_1 = {} #this maps url to app
	with open("URL_Mapping.csv","r",encoding="utf8") as f:
		reader = csv.reader(f,delimiter = ";")
		for row in reader:
			a = row[0]
			a.lower()
			dict_1[a] = row[1]
	f.close()
	
	dict_2 = {} #this maps clien_id to app
	with open("client_id_app_matching.csv","r",encoding="utf8") as f:
		reader = csv.reader(f,delimiter = ";")
		for row in reader:
			a = row[0]
			a.lower()
			dict_2[a] = row[1]
	f.close()
	
	All_the_users = [] #this array contains all the users who have appeared in access logs
	f = open("all_the_users.csv", 'r')
	reader = csv.reader(f,delimiter = ";")
	for row in reader:
		All_the_users.append([row[0],row[1],row[2]])
	f.close()
	
	Users_that_day = []
	f = open("users_that_moment.csv", 'r')
	reader = csv.reader(f,delimiter = ";")
	for row in reader:
		Users_that_day.append([row[0],row[1],row[2]])
	f.close()
	
	Users_that_week = []
	f = open("users_that_moment_weekly.csv", 'r')
	reader = csv.reader(f,delimiter = ";")
	for row in reader:
		Users_that_week.append([row[0],row[1],row[2]])
	f.close()
	
	Users_that_month = []
	f = open("users_that_moment_monthly.csv", 'r')
	reader = csv.reader(f,delimiter = ";")
	for row in reader:
		Users_that_month.append([row[0],row[1],row[2]])
	f.close()
		
	main()

