import csv
import datetime
import sys


def get_week(date): #this returns the week number
	a = date.split("-")
	b = int(a[0])
	c = int(a[1])
	d = int(a[2])
	day = datetime.datetime(b,c,d)
	day += datetime.timedelta(days = 1)
	e = day.isocalendar()[1]
	if (e!=53 or c==12):
		f = a[0] + str(e)
	else:
		f = str(b - 1) + str(e)
	return f

def get_month(date): #this returns the month
	a = date.split("-")
	b = int(a[1])
	if (b==1): return "01" + a[0]
	elif (b==2): return "02" + a[0]
	elif (b==3): return "03" + a[0]
	elif (b==4): return "04" + a[0]
	elif (b==5): return "05" + a[0]
	elif (b==6): return "06" + a[0]
	elif (b==7): return "07" + a[0]
	elif (b==8): return "08" + a[0]
	elif (b==9): return "09" + a[0]
	elif (b==10): return "10" + a[0]
	elif (b==11): return "11" + a[0]
	elif (b==12): return "12" + a[0]
	else :return None

def get_year(date):
	a = date.split("-")
	b = a[0]
	return b

def app_name(com,url): #returns the app of the user
	a = dict_2.get(com)#here com is the app code
	for x in list(dict_1.keys()):
			if x in url:
				return dict_1.get(x)
				break
	if a:
		return a
	else: return "no_app"

def country_name(id): #is the user external or internal
	a = dict.get(id)#here dict is a dictionary which has id as the key and country code and external/internal as value
	if a:
		return a[0]
	else:
		print(id + "is not in reg data")
		return "Unknown_country"

def is_First_time(id,app,lis): #is it a new user for a certain application
	for x in lis:#here lis is in id,country code,app format
		if x[0]==id and x[2]==app:
			return False
	return True

def update_yearly_userlist(filename,lis):
	f = open(filename, 'r')
	text = f.readlines()
	ofile  = open("users_this_year.csv", "w+" , encoding = "utf8", newline='')
	writer = csv.writer(ofile, delimiter=";")
	arr = []
	if text and lis:
		e = text[0].split(" ")
		d = lis[0]
		if (get_year(e[0]) != d[1]):
			del lis[:]
		else: arr = [get_year(e[0])]
	for line in text:
		dat1 = line.split(",")
		id = dat1[3]
		dat = line.split(" ")
		date = dat[0]
		year = get_year(date)
		str1 = dat1[2]
		url = dat1[4]
		app = app_name(str1,url)
		if app != "no_app":
			if year in arr:
				if is_First_time(id,app,lis):
					lis.append([id,year,app])
			else:
				lis = [[id,year,app]]
				arr.append(year)
	for y in lis:
		writer.writerow(y)
	f.close()

def update_daily_userlist(filename,lis):#this program updates the active user file
	f = open(filename, 'r')
	text = f.readlines()
	ofile  = open("users_that_moment.csv", "w+" , encoding = "utf8", newline='')
	writer = csv.writer(ofile, delimiter=";")
	arr = []
	if text and lis:
		e = text[0].split(" ")
		d = lis[0]
		if (e[0] != d[1]):
			del lis[:]
		else: arr = [e[0]]
	for line in text:
		dat1 = line.split(",")
		id = dat1[3]
		dat = line.split(" ")
		date = dat[0]
		str1 = dat1[2]
		url = dat1[4]
		app = app_name(str1,url)
		if date in arr:
			if is_First_time(id,app,lis):
				lis.append([id,date,app])
		else:
			lis = [[id,date,app]]
			arr.append(date)
	for y in lis:
		writer.writerow(y)
	f.close()

def update_weekly_userlist(filename,lis):
	f = open(filename, 'r')
	text = f.readlines()
	ofile  = open("users_that_moment_weekly.csv", "w+" , encoding = "utf8", newline='')
	writer = csv.writer(ofile, delimiter=";")
	arr = []
	if text and lis:
		e = text[0].split(" ")
		d = lis[0]
		if (get_week(e[0]) != d[1]):
			del lis[:]
		else: arr = [get_week(e[0])]
	for line in text:
		dat1 = line.split(",")
		id = dat1[3]
		dat = line.split(" ")
		date = dat[0]
		week = get_week(date)
		str1 = dat1[2]
		url = dat1[4]
		app = app_name(str1,url)
		if week in arr:
			if is_First_time(id,app,lis):
				lis.append([id,week,app])
		else:
			lis = [[id,week,app]]
			arr.append(week)
	for y in lis:
		writer.writerow(y)
	f.close()

def update_monthly_userlist(filename,lis):
	f = open(filename, 'r')
	text = f.readlines()
	ofile  = open("users_that_moment_monthly.csv", "w+" , encoding = "utf8", newline='')
	writer = csv.writer(ofile, delimiter=";")
	arr = []
	if text and lis:
		e = text[0].split(" ")
		d = lis[0]
		if (get_month(e[0]) != d[1]):
			del lis[:]
		else: arr = [get_month(e[0])]
	for line in text:
		dat1 = line.split(",")
		id = dat1[3]
		dat = line.split(" ")
		date = dat[0]
		month = get_month(date)
		str1 = dat1[2]
		url = dat1[4]
		app = app_name(str1,url)
		if month in arr:
			if is_First_time(id,app,lis):
				lis.append([id,month,app])
		else:
			lis = [[id,month,app]]
			arr.append(month)
	for y in lis:
		writer.writerow(y)
	f.close()

def update_userlist(filename,lis):#this program updates the new user file

	f = open(filename, 'r')
	text = f.readlines()
	ofile  = open("all_the_users.csv", "w" , encoding = "utf8", newline='')
	writer = csv.writer(ofile, delimiter=";")

	for line in text:
		dat1 = line.split(",")
		id = dat1[3]
		dat = line.split(" ")
		date = dat[0]
		str1 = dat1[2]
		url = dat1[4]
		app = app_name(str1,url)
		if (app != "no_app"):
			if is_First_time(id,app,lis):
				lis.append([id,date,app])#if it finds a new user it adds it to the list
	print("(^_^)")
	for x in lis:
		writer.writerow(x)

def main():

    print("******()*******")
    args = sys.argv[1:]
    file = args[0]
    print("(o_o)")
    update_userlist(file,All_the_users)
    print("(o_o)")
    update_daily_userlist(file,Users_that_day)
    print("(o_o)")
    update_weekly_userlist(file,Users_that_week)
    print("(o_o)")
    update_monthly_userlist(file,Users_that_month)
    print("(o_o)")
    update_yearly_userlist(file,Users_this_year)
    print("******()*******")

if __name__ == '__main__':
	print("(*_*)")
	dict = {} #this dictionary is for users information from reg data
	with open("id_cc_isExternal.csv","r",encoding="utf8") as f:
		reader = csv.reader(f,delimiter = ";")
		for row in reader:
			a = row[0]
			a.lower()
			dict[a] = [row[1],row[2]]
	f.close()

	dict_1 = {} #this maps url to app
	with open("URL_Mapping.csv","r",encoding="utf8") as f:
		reader = csv.reader(f,delimiter = ";")
		for row in reader:
			a = row[0]
			a.lower()
			dict_1[a] = row[1]
	f.close()

	dict_2 = {} #this maps clien_id to app
	with open("client_id_app_matching.csv","r",encoding="utf8") as f:
		reader = csv.reader(f,delimiter = ";")
		for row in reader:
			a = row[0]
			a.lower()
			dict_2[a] = row[1]
	f.close()

	All_the_users = [] #this array contains all the users who have appeared in access logs
	f = open("all_the_users.csv", 'r')
	reader = csv.reader(f,delimiter = ";")
	for row in reader:
		All_the_users.append([row[0],row[1],row[2]])
	f.close()

	Users_that_day = []
	f = open("users_that_moment.csv", 'r')
	reader = csv.reader(f,delimiter = ";")
	for row in reader:
		Users_that_day.append([row[0],row[1],row[2]])
	f.close()

	Users_that_week = []
	f = open("users_that_moment_weekly.csv", 'r')
	reader = csv.reader(f,delimiter = ";")
	for row in reader:
		Users_that_week.append([row[0],row[1],row[2]])
	f.close()

	Users_that_month = []
	f = open("users_that_moment_monthly.csv", 'r')
	reader = csv.reader(f,delimiter = ";")
	for row in reader:
		Users_that_month.append([row[0],row[1],row[2]])
	f.close()

	Users_this_year = []
	f = open("users_this_year.csv", 'r')
	reader = csv.reader(f,delimiter = ";")
	for row in reader:
		Users_this_year.append([row[0],row[1],row[2]])
	f.close()

	main()
