import os
import sys
import csv
import datetime

#This script is for counting new users and active users.
#The access log required for functionality of this script should be of "yyyy-mm-dd time,INITIAL_LOGIN/REFRESH_TOKEN_LOGIN,app_code,user_id,url" format.
#Also log file must be in ascending order of date
#This script uses the outputs of this code as the input in the processing of next log file,so don't run this script twice for one log file as it will mess up the output.
#all_the_users.csv,users_that_moment.csv,users_that_moment_weekly and users_that_moment_weekly are constantly updated after every program.

def get_week(date): #this returns the week number
	a = date.split("-")
	b = int(a[0])
	c = int(a[1])
	d = int(a[2])
	day = datetime.datetime(b,c,d)
	day += datetime.timedelta(days = 1)
	e = day.isocalendar()[1]
	if (e!=53 or c==12):
		f = a[0] + str(e)
	else:
		f = str(b - 1) + str(e)
	return f

def get_month(date): #this returns the month
	a = date.split("-")
	b = int(a[1])
	if (b==1): return "01" + a[0]
	elif (b==2): return "02" + a[0]
	elif (b==3): return "03" + a[0]
	elif (b==4): return "04" + a[0]
	elif (b==5): return "05" + a[0]
	elif (b==6): return "06" + a[0]
	elif (b==7): return "07" + a[0]
	elif (b==8): return "08" + a[0]
	elif (b==9): return "09" + a[0]
	elif (b==10): return "10" + a[0]
	elif (b==11): return "11" + a[0]
	elif (b==12): return "12" + a[0]
	else :return None

def get_year(date):
	a = date.split("-")
	b = a[0]
	return b

def country_name(id): #is the user external or internal
	global count

	a = dict.get(id)#here dict is a dictionary which has id as the key and country code and external/internal as value
	if a:
		return a[0]
	else:
		count = count + 1
		return "Unknown_country"

def is_First_time(id,app,lis): #is it a new user for a certain application
	for x in lis:#here lis is in id,country code,app format
		if x[0]==id and x[2]==app:
			return False
		elif x[0]==id and app=="not_needed":#if we only need to search the user not the application he was using
			if x[2] not in blocked_list:
				return False
	return True

def isExternal(id): #is the user external or internal
	a = dict.get(id)#here dict is a dictionary which has id as the key and country code and external/internal as value
	if a:
		if (a[1]=="External"):
			return True
		else:
			return False
	else:
		return True

def app_name(com,url): #returns the app of the user
	a = dict_2.get(com)#here com is the app code
	for x in list(dict_1.keys()):
			if x in url:
				return dict_1.get(x)
				break
	if a:
		return a
	else: return "no_app"

def new_active_overall_yearly(filename,lis,lis1):
	p = list(lis)		#this list helps us find new users
	dict = {}			#this will be returned at the end of program
	q = list(lis1)		#this will help us in finding the active users
	f = open(filename, 'r')
	text = f.readlines()
	arr = []
	if text and lis1:
		e = text[0].split(" ")
		d = q[0]
		if (get_year(e[0]) != d[1]):
			del q[:]
		else: arr = [get_year(e[0])]


	for line in text:
		dat1 = line.split(",")
		id = dat1[3]
		dat = line.split(" ")
		date = dat[0]
		year = get_year(date)
		str1 = dat1[2]
		url = dat1[4]
		country = country_name(id)
		app = app_name(str1,url)
		if app not in blocked_list :
			if isExternal(id):
				var = year +";" + country
				if var in dict.keys():
					if is_First_time(id,"not_needed",q):
						a = dict[var]
						a[0] = a[0]+1 #active users increased by 1
						dict[var] = a
						q.append([id,year,""])
						if is_First_time(id,"not_needed",p):
							b = dict[var]
							b[1] = b[1]+1 #new users increased by one
							dict[var] = b
							p.append([id,date,""])
				else:
					if year in arr:
						if is_First_time(id,"not_needed",q):
							q.append([id,year,app])
							if is_First_time(id,"not_needed",p):
								dict[var] = [1,1]
								p.append([id,date,app])
							else: dict[var] = [1,0]
					else:
						arr.append(year)
						q =[[id,year,app]]
						if is_First_time(id,"not_needed",p):
							dict[var] = [1,1]
							p.append([id,date,app])
						else: dict[var] = [1,0]
	print(dict)
	return dict

def app_yearly(filename,lis,lis1):
	p = list(lis)		#this list helps us find new users
	dict = {}			#this will be returned at the end of program
	q = list(lis1)		#this will help us in finding the active users
	f = open(filename, 'r')
	text = f.readlines()
	arr = []
	if text and lis1:
		e = text[0].split(" ")
		d = q[0]
		if (get_year(e[0]) != d[1]):
			del q[:]
		else: arr = [get_year(e[0])]

	for line in text:
		dat1 = line.split(",")
		id = dat1[3]
		dat = line.split(" ")
		date = dat[0]
		str1 = dat1[2]
		url = dat1[4]
		app = app_name(str1,url)
		country = country_name(id)
		year = get_year(date)
		if app not in blocked_list:
			boo = isExternal(id)
			coo = not boo
			if (boo or (coo and app == "Ecoreal XL")):
				var1 = str(year) + ";" + app
				if var1 in dict.keys():
					if is_First_time(id,app,q):
						a = dict[var1]
						a[0] = a[0]+1
						dict[var1] = a
						q.append([id,year,app])
						if is_First_time(id,app,p):
							b = dict[var1]
							b[1] = b[1]+1
							dict[var1] = b
							p.append([id,date,app])
				else:
					if year in arr:
						if is_First_time(id,app,q):
							q.append([id,year,app])
							if is_First_time(id,app,p):
								dict[var1] = [1,1]
								p.append([id,date,app])
							else: dict[var1] = [1,0]
					else:
						arr.append(year)
						q =[[id,year,app]]
						if is_First_time(id,app,p):
							dict[var1] = [1,1]
							p.append([id,date,app])
						else: dict[var1] = [1,0]
	return dict

def app_active_new_yearly(filename,lis,lis1):
	p = list(lis)		#this list helps us find new users
	dict = {}			#this will be returned at the end of program
	q = list(lis1)		#this will help us in finding the active users
	f = open(filename, 'r')
	text = f.readlines()
	arr = []
	if text and lis1:
		e = text[0].split(" ")
		d = q[0]
		if (get_year(e[0]) != d[1]):
			del q[:]
		else: arr = [get_year(e[0])]

	for line in text:
		dat1 = line.split(",")
		id = dat1[3]
		dat = line.split(" ")
		date = dat[0]
		str1 = dat1[2]
		url = dat1[4]
		app = app_name(str1,url)
		country = country_name(id)
		year = get_year(date)
		if app not in blocked_list:
			boo = isExternal(id)
			coo = not boo
			if (boo or (coo and app == "Ecoreal XL")):
				var1 = str(year) + ";" + app + ";" + country
				if var1 in dict.keys():
					if is_First_time(id,app,q):
						a = dict[var1]
						a[0] = a[0]+1
						dict[var1] = a
						q.append([id,year,app])
						if is_First_time(id,app,p):
							b = dict[var1]
							b[1] = b[1]+1
							dict[var1] = b
							p.append([id,date,app])
				else:
					if year in arr:
						if is_First_time(id,app,q):
							q.append([id,year,app])
							if is_First_time(id,app,p):
								dict[var1] = [1,1]
								p.append([id,date,app])
							else: dict[var1] = [1,0]
					else:
						arr.append(year)
						q =[[id,year,app]]
						if is_First_time(id,app,p):
							dict[var1] = [1,1]
							p.append([id,date,app])
						else: dict[var1] = [1,0]
	return dict

def country_app_daily(filename,lis,lis1):
		p = list(lis)		#this list helps us find new users
		dict = {}			#this will be returned at the end of program
		q = list(lis1)		#this will help us in finding the active users
		f = open(filename, 'r')
		text = f.readlines()
		arr = []
		#rest of the code is similar to the fuction above
		if text and lis1:
			e = text[0].split(" ")
			d = q[0]
			arr.append(e[0])
			if (e[0] != d[1]):
				del q[:]

		for line in text:
			dat1 = line.split(",")
			id = dat1[3]
			dat = line.split(" ")
			date = dat[0]
			str1 = dat1[2]
			url = dat1[4]
			country = country_name(id)
			app = app_name(str1,url)
			if app not in blocked_list:
				boo = isExternal(id)
				coo = not boo
				if (boo or (coo and app == "Ecoreal XL")):
					var1 = date + ";" +country +";" +app
					if var1 in dict.keys():
						if is_First_time(id,app,q):
							a = dict[var1]
							a[0] = a[0]+1
							dict[var1] = a
							q.append([id,date,app])
							if is_First_time(id,app,p):
								b = dict[var1]
								b[1] = b[1]+1
								dict[var1] = b
								p.append([id,date,app])
					else:
						if date in arr:
							if is_First_time(id,app,q):
								q.append([id,date,app])
								if is_First_time(id,app,p):
									dict[var1] = [1,1]
									p.append([id,date,app])
								else: dict[var1] = [1,0]
						else:
							arr.append(date)
							q =[[id,date,app]]
							if is_First_time(id,app,p):
								dict[var1] = [1,1]
								p.append([id,date,app])
							else: dict[var1] = [1,0]

		return dict

def country_app_weekly(filename,lis,lis1):
	p = list(lis)		#this list helps us find new users
	dict = {}			#this will be returned at the end of program
	q = list(lis1)		#this will help us in finding the active users
	f = open(filename, 'r')
	text = f.readlines()
	arr = []
	#rest of the code is similar to the fuction above
	if text and lis1:
		e = text[0].split(" ")
		d = q[0]
		arr.append(get_week(e[0]))
		if (get_week(e[0]) != d[1]):
			del q[:]

	for line in text:
		dat1 = line.split(",")
		id = dat1[3]
		dat = line.split(" ")
		date = dat[0]
		str1 = dat1[2]
		url = dat1[4]
		week = get_week(date)
		country = country_name(id)
		app = app_name(str1,url)
		if app not in blocked_list:
			boo = isExternal(id)
			coo = not boo
			if (boo or (coo and app == "Ecoreal XL")):
				var1 = week + ";" +country +";" +app
				if var1 in dict.keys():
					if is_First_time(id,app,q):
						a = dict[var1]
						a[0] = a[0]+1
						dict[var1] = a
						q.append([id,week,app])
						if is_First_time(id,app,p):
							b = dict[var1]
							b[1] = b[1]+1
							dict[var1] = b
							p.append([id,date,app])
				else:
					if week in arr:
						if is_First_time(id,app,q):
							q.append([id,week,app])
							if is_First_time(id,app,p):
								dict[var1] = [1,1]
								p.append([id,date,app])
							else: dict[var1] = [1,0]
					else:
						arr.append(week)
						q =[[id,week,app]]
						if is_First_time(id,app,p):
							dict[var1] = [1,1]
							p.append([id,date,app])
						else: dict[var1] = [1,0]

	return dict

def country_app_monthly(filename,lis,lis1):
	p = list(lis)		#this list helps us find new users
	dict = {}			#this will be returned at the end of program
	q = list(lis1)		#this will help us in finding the active users
	f = open(filename, 'r')
	text = f.readlines()
	arr = []

	#rest of the code is similar to the fuction above
	if text and lis1:
		e = text[0].split(" ")
		d = q[0]
		arr.append(get_month(e[0]))
		if (get_month(e[0]) != d[1]):
			del q[:]

	for line in text:
		dat1 = line.split(",")
		id = dat1[3]
		dat = line.split(" ")
		date = dat[0]
		str1 = dat1[2]
		url = dat1[4]
		month = get_month(date)
		country = country_name(id)
		app = app_name(str1,url)
		if app not in blocked_list:
			boo = isExternal(id)
			coo = not boo
			if (boo or (coo and app == "Ecoreal XL")):
				var1 = month + ";" +country +";" +app
				if var1 in dict.keys():
					if is_First_time(id,app,q):
						a = dict[var1]
						a[0] = a[0]+1
						dict[var1] = a
						q.append([id,month,app])
						if is_First_time(id,app,p):
							b = dict[var1]
							b[1] = b[1]+1
							dict[var1] = b
							p.append([id,date,app])
				else:
					if month in arr:
						if is_First_time(id,app,q):
							q.append([id,month,app])
							if is_First_time(id,app,p):
								dict[var1] = [1,1]
								p.append([id,date,app])
							else: dict[var1] = [1,0]
					else:
						arr.append(month)
						q =[[id,month,app]]
						if is_First_time(id,app,p):
							dict[var1] = [1,1]
							p.append([id,date,app])
						else: dict[var1] = [1,0]

	return dict

def main():
	global count
	count = 0
	args = sys.argv[1:]
	vae = str(datetime.datetime.now())
	vaes = vae[:10]
	folder = vaes+"/"
	os.makedirs(os.path.dirname(folder), exist_ok=True)

	print("(o_o)")
	args = sys.argv[1:]
	print("(o_o)")

	if not args:
		print ('NO FILE')
		sys.exit(1)

	file = args[0]


	A7 = new_active_overall_yearly(file,All_the_users,Users_this_year)
	print(str(count) + " entries of access logs are not in reg data")
	print("(o_o)")
	A8 = app_active_new_yearly(file,All_the_users,Users_this_year)
	print("(o_o)")
	A9 = country_app_daily(file,All_the_users,Users_that_day)
	print("(o_o)")
	AA = country_app_weekly(file,All_the_users,Users_that_week)
	print("(o_o)")
	AB = country_app_monthly(file,All_the_users,Users_that_month)
	print("(o_o)")
	AC = app_yearly(file,All_the_users,Users_this_year)
	print("(o_o)")



	ofile1  = open(vaes +"/country-total.csv", "w+" , encoding = "utf8", newline='')
	writer1 = csv.writer(ofile1, delimiter=";")
	writer1.writerow(["year","country","Active_Users","New_Users"])
	ofile2  = open(vaes +"/app-country-total.csv", "w+" , encoding = "utf8", newline='')
	writer2 = csv.writer(ofile2, delimiter=";")
	writer2.writerow(["year","app","country","Active_Users","New_Users"])
	ofile3  = open(vaes +"/app-country-daily.csv", "w+" , encoding = "utf8", newline='')
	writer3 = csv.writer(ofile3, delimiter=";")
	writer3.writerow(["date","country","app","Active_Users","New_Users"])
	ofile4  = open(vaes +"/app-country-weekly.csv", "w+" , encoding = "utf8", newline='')
	writer4 = csv.writer(ofile4, delimiter=";")
	writer4.writerow(["week","country","app","Active_Users","New_Users"])
	ofile5  = open(vaes +"/app-country-monthly.csv", "w+" , encoding = "utf8", newline='')
	writer5 = csv.writer(ofile5, delimiter=";")
	writer5.writerow(["month","country","app","Active_Users","New_Users"])
	ofile6  = open(vaes +"/app-yearly.csv", "w+" , encoding = "utf8", newline='')
	writer6 = csv.writer(ofile6, delimiter=";")
	writer6.writerow(["year","app","Active_Users","New_Users"])
	print("(o_o)")

	for q in A7.keys():
		u = A7[q]
		temp = q.split(";")
		writer1.writerow([temp[0],temp[1],u[0],u[1]])
	print("(o_o)")

	for w in A8.keys():
		i = A8[w]
		temp = w.split(";")
		writer2.writerow([temp[0],temp[1],temp[2],i[0],i[1]])
	for r in A9.keys():
		p = A9[r]
		temp = r.split(";")
		writer3.writerow([temp[0],temp[1],temp[2],p[0],p[1]])
	print("(o_o)")

	for t in AA.keys():
		l = AA[t]
		temp = t.split(";")
		writer4.writerow([temp[0],temp[1],temp[2],l[0],l[1]])
	for y in AB.keys():
		k = AB[y]
		temp = y.split(";")
		writer5.writerow([temp[0],temp[1],temp[2],k[0],k[1]])
	for z in AC.keys():
		k = AC[z]
		temp = z.split(";")
		writer6.writerow([temp[0],temp[1],k[0],k[1]])

	print("(o_o)")

if __name__ == '__main__':

	blocked_list = ["no_app","csmdemo","easyline","Ecoreal MV","Ecoreal MV - NPR","Facility Insight","MySchneider","MySchneider - NPR","MySchneider Retailer","myschneider3dlive","mysmartoperator","omega","Rapsody","resiplatform","service-manager","SR","SR - NPR","smartpanel"]

	dict = {} #this dictionary is for users information from reg data
	with open("id_cc_isExternal.csv","r",encoding="utf8") as f:
		reader = csv.reader(f,delimiter = ";")
		for row in reader:
			a = row[0]
			a.lower()
			dict[a] = [row[1],row[2]]
	f.close()

	dict_1 = {} #this maps url to app
	with open("URL_Mapping.csv","r",encoding="utf8") as f:
		reader = csv.reader(f,delimiter = ";")
		for row in reader:
			a = row[0]
			a.lower()
			dict_1[a] = row[1]
	f.close()

	dict_2 = {} #this maps clien_id to app
	with open("client_id_app_matching.csv","r",encoding="utf8") as f:
		reader = csv.reader(f,delimiter = ";")
		for row in reader:
			a = row[0]
			a.lower()
			dict_2[a] = row[1]
	f.close()

	All_the_users = [] #this array contains all the users who have appeared in access logs
	f = open("all_the_users.csv", 'r')
	reader = csv.reader(f,delimiter = ";")
	for row in reader:
		All_the_users.append([row[0],row[1],row[2]])
	f.close()

	Users_this_year = []
	f = open("users_this_year.csv", 'r')
	reader = csv.reader(f,delimiter = ";")
	for row in reader:
		Users_this_year.append([row[0],row[1],row[2]])
	f.close()

	Users_that_day = []
	f = open("users_that_moment.csv", 'r')
	reader = csv.reader(f,delimiter = ";")
	for row in reader:
		Users_that_day.append([row[0],row[1],row[2]])
	f.close()

	Users_that_week = []
	f = open("users_that_moment_weekly.csv", 'r')
	reader = csv.reader(f,delimiter = ";")
	for row in reader:
		Users_that_week.append([row[0],row[1],row[2]])
	f.close()

	Users_that_month = []
	f = open("users_that_moment_monthly.csv", 'r')
	reader = csv.reader(f,delimiter = ";")
	for row in reader:
		Users_that_month.append([row[0],row[1],row[2]])
	f.close()

	main()
